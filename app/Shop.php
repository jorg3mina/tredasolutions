<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Shop extends Model
{

    protected $fillable = [
        'nombre','fecha_apertura'
    ];


    public function products()
    {
        return $this->hasMany('App\Product');
    }
}
