<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $fillable = [
        'nombre','sku','descripcion','valor','imagen','shops_id'
    ];


    public function shops()
    {
        return $this->belongsTo('App\Shop');
    }
}
