@extends('layouts.app')
 
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">LIST SHOPS</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12 margin-tb">
                                <div class="pull-right">
                                    <a class="btn btn-success" href="{{ route('shops.create') }}"> Create New Shop</a>
                                </div>
                            </div>
                        </div>

                        <br>
                    
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                    
                        <table class="table table-bordered">
                            <tr>
                                <th>No</th>
                                <th>Nombre</th>
                                <th>Fecha Apertura</th>
                                <th width="280px">Action</th>
                            </tr>
                            @foreach ($shops as $shop)
                            <tr>
                                <td>{{ $shop->id }}</td>
                                <td>{{ $shop->nombre }}</td>
                                <td>{{ $shop->fecha_apertura }}</td>
                                <td>
                                    <form action="{{ route('shops.destroy',$shop->id) }}" method="POST">
                        
                                        <a class="btn btn-primary" href="{{ route('shops.edit',$shop->id) }}">Edit</a>
                    
                                        @csrf
                                        @method('DELETE')
                        
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </table>
                    
                        {!! $shops->links() !!}
                    </div>
                </div>
            </div>
        </div>  
    </div>      
@endsection