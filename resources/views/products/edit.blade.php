@extends('layouts.app')
   
@section('content')
    <div class="container"> 
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">EDIT SHOP</div>
                    <div class="card-body">                     
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    
                        <form enctype="multipart/form-data" action="{{ route('products.update',$product->id) }}" method="POST">
                            @csrf
                            @method('PUT')
                    
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <strong>Nombre:</strong>
                                        <input type="text" name="nombre" value="{{ $product->nombre }}" class="form-control" placeholder="Nombre">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <strong>Sku:</strong>
                                        <input type="text" name="sku" value="{{ $product->sku }}" class="form-control" placeholder="Sku">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <strong>Descripcion:</strong>
                                        <input type="text" name="descripcion" value="{{ $product->descripcion }}" class="form-control" placeholder="Descripcion">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <strong>Valor:</strong>
                                        <input type="text" name="valor" value="{{ $product->valor }}" class="form-control" placeholder="Valor">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <strong>Tienda:</strong>
                                        <select name="shops_id" class="form-control">
                                            @foreach($shops as $shop)
                                                <option value="{{ $shop->id }}"
                                                @if ($product->shops== $shop)
                                                    selected
                                                @endif 

                                                > {{$shop->nombre }}</option> 
                                            @endforeach 
                                            
                                        </select> 
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <strong>Imagen:</strong>
                                        <input type="text" value="{{ $product->imagen }}" class="form-control" readonly><br>
                                        <input type="file" name="imagen">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a class="btn btn-primary" href="{{ route('products.index') }}"> Back</a>
                                </div>
                            </div>
                    
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection