@extends('layouts.app')
 
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">LIST PRODUCTS</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12 margin-tb">
                                <div class="pull-right">
                                    <a class="btn btn-success" href="{{ route('products.create') }}"> Create New Shop</a>
                                </div>
                            </div>
                        </div>

                        <br>
                    
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                    
                        <table class="table">
                            <tr>
                                <th>No</th>
                                <th>Nombre</th>
                                <th>Sku</th>
                                <th>Descripcion</th>
                                <th>Valor</th>
                                <th>Tienda</th>
                                <th>Imagen</th>
                                <th width="280px">Action</th>
                            </tr>
                            @foreach ($products as $product)
                            <tr>
                                <td>{{ $product->id }}</td>
                                <td>{{ $product->nombre }}</td>
                                <td>{{ $product->sku }}</td>
                                <td>{{ $product->descripcion }}</td>
                                <td>{{ $product->valor }}</td>
                                <td>{{ $product->shops->nombre }}</td>
                                <td>
                                    <img class="img-rounded" width="120" height="150" src="storage/{{ $product->imagen }}" alt="image">
                                <td>
                                    <form action="{{ route('products.destroy',$product->id) }}" method="POST">
                        
                                        <a class="btn btn-primary" href="{{ route('products.edit',$product->id) }}">Edit</a>
                    
                                        @csrf
                                        @method('DELETE')
                        
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </table>
                    
                        {!! $products->links() !!}
                    </div>
                </div>
            </div>
        </div>  
    </div>      
@endsection