<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id()->autoIncrement();
            $table->string('nombre')->required();
            $table->integer('sku')->unique()->required();
            $table->string('descripcion')->required();
            $table->double('valor', 10, 2)->required();
            $table->text('imagen')->required();
            $table->unsignedBigInteger('shops_id'); 
            $table->foreign('shops_id')
                ->references('id')
                ->on('shops');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
